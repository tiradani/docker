# Supported tags and respective `Dockerfile` links

-	[`1.2.8p26` (*mbabik/etf:1.2.8p26*)](https://gitlab.cern.ch/etf/docker/blob/master/etf/Dockerfile)
-	[`1.5.1` (*mbabik/stompclt:1.5.1*)](https://gitlab.cern.ch/etf/docker/blob/master/stompclt/Dockerfile)

For more information about this image and its history, please see [the relevant manifest file (`mbabik/etf`)]. This image is updated via [pull requests to the `etf/docker` GitLab repo](https://gitlab.cern.ch/etf/docker/tree/master).
The official repository for the images is at [docker.io](https://cloud.docker.com/swarm/mbabik/repository/docker/mbabik/etf/general)

# ETF
Experiments Test Framework (ETF) is a measurement middleware for functional/availability testing of the resources. It’s primary purpose is testing the services to determine availability delivered by resource providers, but it has been successfully used in many deployment and software campaigns, security testing, evaluation of new technologies as well as a generic collector (crawler). It has the following features:

-    generic framework based on [Nagios-core](http://www.nagios.org) and [check_mk](https://mathias-kettner.de/check_mk.html)
-    pluggable interface for tests based on monitoring plugins standard
-    wide range of available plugins for grid infrastructure testing
-    python API for auto-generating nagios-core configuration from custom topologies and support for quick turn around in plugin-to-test cycle
-    site notifications/alerts, on-demand test re-scheduling and various interfaces to help with debugging

More information is available in the official documentation at http://etf.cern.ch/docs/latest/

# ETF Image Documentation

ETF Dockerfiles can be easily re-used to build a single site check_mk instance, but they do come with the following configuration tweaks: 
-  HTTPS-only access and authorisation based on certificates (certs and CAs are required as a volume)
-  Publishing all nagios events to `ActiveMQ` via open-source `stompclt` and `ETF nagios-stream` (stompclt config required as a volume)
-  Three volumes are expected: certs and CAs to be mounted under /etc/grid-security; check_mk config to be mounted under /opt/omd/site/etf/etc/check_mk and stompctl configuration to be mounted to /etc/stompclt
-  The image contains the following Check_MK configuration changes:
   -  TMPFS off; APACHE_MODE own; DEFAULT_GUI check_mk; CORE nagios
   -  LIVESTATUS_TCP on;  NAGIOS_THEME exfoliation;  MULTISITE_AUTHORISATION off
   -  MULTISITE_COOKIE_AUTH off
  
# Sample run
```console
$ docker run mbabik/etf:1.2.8p19

$ docker exec --name etf_ps ps -ef # to run command inside container
$ docker logs -f etf_ps # to see the logs
$ docker exec -it etf_ps /bin/bash # to get shell inside container

```


# Supported Docker versions

This image is officially supported on Docker CE version 17.03.0.

Support for older versions is provided on a best-effort basis.

Please see [the Docker installation documentation](https://docs.docker.com/installation/) for details on how to upgrade your Docker daemon.

# User Feedback

## Issues

If you have any problems with or questions about this image, please contact us through a [GitLab issue](https://gitlab.cern.ch/etf/docker/issues).

## Contributing

You are invited to contribute new features, fixes, or updates, large or small; we are always thrilled to receive pull requests, and do our best to process them as fast as we can.

Before you start to code, we recommend discussing your plans through a [GitLab issue](https://gitlab.cern.ch/etf/docker/issues), especially for more ambitious contributions. This gives other contributors a chance to point you in the right direction, give you feedback on your design, and help you find out if someone else is working on the same thing.

# Installation

## VM Host

To install ETF, you will need a VM with any OS that supports Docker; such as CentOS7

Minimum resource requirements are..

* 4-6 CPUs
* 8G memory
* 16G disk

### Docker Engine

Follow the official [docker installation doc](https://docs.docker.com/engine/installation/) (not from the RHEL repo) to install docker engine.

For CentOS7 as root

```bash
yum-config-manager \
    --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce
```

Before you start the docker engine, you might want to add any VM specific configuration. For example, your VM might be using /usr/local as a primary partition for your VM (like at GOC). If so, you should have something like following..

```bash
mkdir /etc/docker
```

`/etc/docker/daemon.json`

```json
{
        "graph": "/usr/local/docker"
}

```

Enable & start the docker engine.

```
$ systemctl enable docker
$ systemctl start docker
```

You should install logrotate for docker container log

/etc/logrotate.d/docker-container
```
/docker/containers/*/*.log {
  rotate 7
  daily
  compress
  size=1M
  missingok
  delaycompress
  copytruncate
}
```

### Configuration

Before we start installing ETF, you should prepare your configuration files first. You can bootstrap it by
reguesting ETF's default configuration files from the developer.

The following configuration files and volumes need to be prepared in advance:
- /etc/grid-security containing host/cert/key.pem and certificates CA directory
- /etc/check_mk with check_mk configuration for a site
- /etc/stompclt with stomplct_ps.cfg 
- optionally an empty directory that will contain RRD files produced by pnp4nagios 

#### Host Certificates

You will need SSL certificates for https access. On /etc/grid-security/host, you should see your host certificate with following file names. 

```bash
$ ls /etc/grid-security/host
cert.pem 
key.pem
```

If not, please request for new certificate (at IU, see https://kb.iu.edu/d/bevd). 
You will also need CA files under /etc/grid-security/certificate in order for x509 authentication to work. Please see [osg-ca-cert](https://twiki.grid.iu.edu/bin/view/Documentation/Release3/InstallCertAuth) for more information.

#### System user

ETF runs as a system user, so one needs to be created on the host VM. In addition, it must have access to the configuration files.

```bash
$ useradd -rM etf
$ chown etf /etc/grid-security/host/*
```

### Deployment

Now we have all configuration files necessary to start deploying the ETF service.

1. First, create a docker network

    ```bash
    docker network create etf
    ```

2. Create directory to persist RRD graphs and messages queue

    ```bash
    mkdir -p  /usr/local/perfdata
    chmod a+xw /usr/local/perfdata/
    mkdir -p /var/spool/nstream/outgoing
    chmod a+xw /var/spool/nstream/outgoing
    
    ```

3. Start ETF

    ```bash
    $ id etf
    uid=993(etf) gid=907(etf) groups=907(etf)
    $ docker run -d -p 443:443 -p 80:80 --net=etf --name etf_ps \
                 -v /usr/local/perfdata/:/omd/sites/etf/var/pnp4nagios/perfdata \
                 -v /etc/grid-security/:/etc/grid-security \ 
                 -v /etc/check_mk:/opt/omd/sites/etf/etc/check_mk \
                 -v /etc/stompclt:/etc/stompclt \
                 -e "CHECK_MK_USER_ID=993"  mbabik/etf:1.2.8p23
    ```

    > CHECK_MK_USER_ID should match UID of the system user created for this purpose
    > mbabik/etf:1.2.8p23 should be replaced with the latest version available from https://store.docker.com/community/images/mbabik/etf/tags
    
4. Start STOMPCLT (publisher)
    ```bash
    $ docker run -d --restart always --net=etf --name stompclt \
                 -v /var/spool/nstream/outgoing:/var/spool/nstream/outgoing \
                 -v /etc/grid-security/:/etc/grid-security  \
                 -v /etc/stompclt:/etc/stompclt  \
                 mbabik/stompclt:1.5.1 --conf /etc/stompclt/stompclt_ps.cfg

Now you should see both containers running.

```bash
# docker ps
CONTAINER ID        IMAGE                   COMMAND                  CREATED             STATUS              PORTS                 NAMES
fbce86c70c70        mbabik/stompclt:1.5.1   "/usr/bin/stompclt..."   3 minutes ago       Up 3 minutes                              stompclt
f94119e1bd6c        mbabik/etf:1.2.8p23     "/bin/sh -c /docke..."   17 minutes ago      Up 17 minutes       0.0.0.0:80->80/tcp... etf_ps


```

# Update

The following list of commands shows how to upgrade to a newer version of the service.
```bash
$ docker stop etf_ps
$ docker rm etf_ps
$ docker stop stompclt
$ docker rm stompclt
$ docker run -d -p 443:443 -p 80:80 --net=etf --name etf_ps \
                 -v /usr/local/perfdata/:/omd/sites/etf/var/pnp4nagios/perfdata \
                 -v /etc/grid-security/:/etc/grid-security \ 
                 -v /etc/check_mk:/opt/omd/sites/etf/etc/check_mk \
                 -v /etc/stompclt:/etc/stompclt \
                 -e "CHECK_MK_USER_ID=993"  mbabik/etf:<new_tag>
$ docker run -d --restart always --net=etf --name stompclt \
                 -v /var/spool/nstream/outgoing:/var/spool/nstream/outgoing \
                 -v /etc/grid-security/:/etc/grid-security  \
                 -v /etc/stompclt:/etc/stompclt  \
                 mbabik/stompclt:<new_tag> --conf /etc/stompclt/stompclt_ps.cfg
```

Rollback to previous version is possible by following the above commands while replacing ```<new_tag>``` with a previous version. 
ETF and STOMPCLT can be updated independently, so if there is a new version of just stompclt, it's enough to execute just stompclt update. 
Current list of supported versions can be found at:
- [ETF versions](https://store.docker.com/community/images/mbabik/etf/tags)
- [STOMPCLT versions](https://store.docker.com/community/images/mbabik/stompclt/tags)
