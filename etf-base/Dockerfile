#FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
FROM centos:7.4.1708

LABEL maintainer="Marian Babik <Marian.Babik@cern.ch>"
LABEL description="ETF base image"
LABEL version="1.0"

ARG check_mk_package="check-mk-raw-1.4.0p36-el7-83.x86_64.rpm"
ARG check_mk_version="1.4.0p36"
ARG check_mk_site="etf"

ENV LIVESTATUS_TCP_PORT=6557
ENV CHECK_MK_SITE=${check_mk_site:-etf}
ENV CHECK_MK_VERSION=${check_mk_version}
ENV SHELL=bash

COPY ./config/etf.repo /etc/yum.repos.d/etf.repo

RUN yum -y install epel-release
#RUN yum -y update
# Utils
RUN yum -y install vim-common vim-enhanced yum-utils nmap-ncat libdbi mod_ssl wget mailx sendmail cronie
# ETF
RUN yum -y install python-nap ncgx python-vofeed-api
RUN yum -y install nagios-stream
# Check_MK
ADD https://mathias-kettner.de/support/$check_mk_version/$check_mk_package /tmp/$check_mk_package
RUN yum -y localinstall /tmp/$check_mk_package
RUN yum -y localinstall /opt/omd/versions/default/share/check_mk/agents/check-mk-agent-*.noarch.rpm

RUN omd create $check_mk_site
RUN omd config $check_mk_site set TMPFS off
RUN omd config $check_mk_site set APACHE_MODE own
RUN omd config $check_mk_site set DEFAULT_GUI check_mk
RUN omd config $check_mk_site set CORE nagios
RUN omd config $check_mk_site set LIVESTATUS_TCP on
RUN omd config $check_mk_site set NAGIOS_THEME exfoliation
RUN omd config $check_mk_site set MULTISITE_AUTHORISATION off
RUN omd config $check_mk_site set MULTISITE_COOKIE_AUTH off


# Link to standard nagios pipe
RUN mkdir -p /var/nagios/rw && ln -s /omd/sites/$check_mk_site/tmp/run/nagios.cmd /var/nagios/rw/nagios.cmd

# pnp4nagios tuning
RUN sed -i  "s/TRUE/FALSE/" /opt/omd/sites/$check_mk_site/etc/pnp4nagios/config.php
RUN sed -e "s|RRD_HEARTBEAT = 8460|RRD_HEARTBEAT = 25200|g" \
        -i /opt/omd/sites/$check_mk_site/etc/pnp4nagios/process_perfdata.cfg

# python env
RUN sed -e 's|$OMD_ROOT/local/lib/python"|$OMD_ROOT/local/lib/python:/usr/lib/python2.7/site-packages/:/usr/lib64/python2.7/site-packages"|' \
        -i /opt/omd/sites/$check_mk_site/.profile

# httpd ssl auth config
RUN rm -f /etc/httpd/conf.d/zzz_omd.conf
COPY ./config/ssl.conf /etc/httpd/conf.d/ssl.conf
COPY ./config/auth.conf /opt/omd/sites/$check_mk_site/etc/apache/conf.d/auth.conf

# ETF config
RUN chown -R $check_mk_site.$check_mk_site /etc/ncgx
RUN chown -R $check_mk_site.$check_mk_site /var/cache/ncgx
RUN mkdir /var/cache/nap && chown -R $check_mk_site.$check_mk_site /var/cache/nap
COPY ./config/nagios.cfg /omd/sites/$check_mk_site/etc/nagios/nagios.cfg
COPY ./config/ncgx.cron /opt/omd/sites/$check_mk_site/etc/cron.d/ncgx
COPY ./config/etf.cron /etc/cron.d/etf_cleanup

EXPOSE 80 443 6557
