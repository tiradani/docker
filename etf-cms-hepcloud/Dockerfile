FROM gitlab-registry.cern.ch/etf/docker/etf-exp:latest

LABEL maintainer="Marian Babik <Marian.Babik@cern.ch>"
LABEL description="WLCG ETF CMS"
LABEL version="1.0"

ENV NSTREAM_ENABLED=0

# OSG Middleware
RUN rpm -Uvh https://repo.opensciencegrid.org/osg/3.4/osg-3.4-el7-release-latest.rpm

# Core packages
RUN yum -y install voms globus-gsi-sysconfig globus-gsi-cert-utils globus-gssapi-gsi globus-gss-assist \
                   globus-gsi-proxy-core globus-gsi-credential globus-gsi-callback globus-gsi-openssl-error \
                   globus-openssl-module globus-gsi-proxy-ssl globus-callout

# Condor client
RUN yum -y install condor condor-python

# MW env
COPY ./config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile

# VOMS config
RUN mkdir -p /etc/vomses/
COPY ./config/cms-lcg-voms2.cern.ch /etc/vomses/
COPY ./config/cms-voms2.cern.ch /etc/vomses/
#RUN mkdir -p /etc/grid-security/vomsdir/cms/
#COPY ./config/lcg-voms2.cern.ch.lsc /etc/grid-security/vomsdir/cms/
#COPY ./config/voms2.cern.ch.lsc /etc/grid-security/vomsdir/cms/

# ETF base plugins
RUN yum -y install nagios-plugins-wlcg-condor ncgx-config-wlcg nagios-plugins-globus nagios-plugins

# ETF WN payload
RUN yum -y install nagios-plugins-wlcg-org.cms

# ETF job submission setup
RUN yum -y install python-jess python-nap && chmod 755 /usr/lib64/nagios/plugins/check_js
COPY ./config/check_condor.cfg /etc/ncgx/metrics.d/
COPY ./config/metrics.cfg /etc/ncgx/metrics.d/wlcg_cms.cfg

# ETF toplogy config
COPY ./config/etf_plugin_cms_hepcloud.py /usr/lib/ncgx/x_plugins/

# ETF streaming
RUN mkdir -p /var/spool/nstream/outgoing && chmod 777 /var/spool/nstream/outgoing
RUN mkdir /etc/stompclt
COPY ./config/ocsp_handler.cfg /etc/nstream/

# CMS config
COPY config/cms_checks.cfg /etc/ncgx/conf.d/

# ETF config
COPY ./config/service_template.tpl /etc/ncgx/templates/
COPY ./config/ncgx.cfg /etc/ncgx/

EXPOSE 80 443 6557
COPY ./docker-entrypoint.sh /
ENTRYPOINT /docker-entrypoint.sh
