#!/bin/bash
set -e

cat << "EOF"
      ('-.   .-') _
    _(  OO) (  OO) )
   (,------./     '._    ,------.
    |  .---'|'--...__)('-| _.---'
    |  |    '--.  .--'(OO|(_\
   (|  '--.    |  |   /  |  '--.
    |  .--'    |  |   \_)|  .--'
    |  `---.   |  |     \|  |_)
    `------'   `--'      `--'
===================================
EOF
ncgx_version=`rpm -q --qf "%{VERSION}-%{RELEASE}" ncgx`
echo "ETF version: ${ncgx_version} Copyright CERN 2016"
echo "License: https://gitlab.cern.ch/etf/ncgx/blob/master/LICENSE"
echo "Check_MK version: $CHECK_MK_VERSION"
echo "Copyright by Mathias Kettner (https://mathias-kettner.de/check_mk.html)"
plugins=`rpm -qa | grep nagios-plugins`
echo "Plugins:" 
echo "${plugins}"
echo ""
echo "Starting xinetd ..."
export XINETD_LANG="en_US" && /usr/sbin/xinetd -stayalive -pidfile /var/run/xinetd.pid
if [[ -n $CHECK_MK_USER_ID ]] ; then
   echo "Changing $CHECK_MK_SITE uid to $CHECK_MK_USER_ID"
   /usr/sbin/usermod -u $CHECK_MK_USER_ID $CHECK_MK_SITE
   chown -R $CHECK_MK_SITE /etc/ncgx /var/cache/ncgx /var/cache/nap
fi
if [[ -n $CHECK_MK_GROUP_ID ]] ; then
   echo "Creating group with gid $CHECK_MK_GROUP_ID"
   /usr/sbin/groupadd -g $CHECK_MK_GROUP_ID sec
   /usr/sbin/groupmems -g sec -a $CHECK_MK_SITE
fi
cp /etc/ncgx/templates/generic/handlers.cfg /opt/omd/sites/etf/etc/nagios/conf.d/
omd start
rm -f /opt/omd/sites/etf/etc/nagios/conf.d/handlers.cfg 
su etf -c "ncgx"
su - etf -c "cmk -II; cmk -O"
echo "Starting Apache ..."
/usr/sbin/httpd -DFOREGROUND

