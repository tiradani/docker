#!/bin/bash
set -e

cat << "EOF"
  _   _ _____ ____                _
 | | | |_   _/ ___|___  _ __   __| | ___  _ __
 | |_| | | || |   / _ \| '_ \ / _` |/ _ \| '__|
 |  _  | | || |__| (_) | | | | (_| | (_) | |
 |_| |_| |_| \____\___/|_| |_|\__,_|\___/|_|
================================================
EOF
condor_version=`rpm -q --qf "%{VERSION}-%{RELEASE}" condor`
echo "HTCondor version: ${condor_version} Copyright © 1990-2012 HTCondor Team"
echo "Computer Sciences Department, University of Wisconsin-Madison, WI."
echo "License: https://research.cs.wisc.edu/htcondor/license.html"
echo ""
echo "Configuring ..."
if [ "$IPV6_ENABLED" = true ] ; then
    echo "HTCondor configured as IPv6-only"
    cp -f /etc/condor_config_ipv6 /etc/condor/condor_config
fi
echo "Starting condor_master  ..."
/usr/sbin/condor_master -f -t

